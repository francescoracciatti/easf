# ASF

This project bundles the Attack Simulation Framework with the WSN simulator Castalia. 
TBD 

## Getting Started

TBD

### Prerequisites

TBD

### Installing

TBD

### Running the tests

TBD

### Usage

TBD

### Samples

TBD
 
### Deployment

TBD

## Built With

### Development and testing environment

* macOS High Sierra 10.13.6
* [CLion](https://www.jetbrains.com/pycharm/) 2019.3.3
* [Python](https://www.python.org/downloads/) 3.7.5

### Deployment environment

* [VirtualBox](https://www.virtualbox.org/wiki/Downloads) 6.0.14
* [Ubuntu](https://ubuntu.com/) 18.04
* [Python](https://www.python.org/downloads/) 3.7.5
* [OMNeT++](https://omnetpp.org/) 5.6
* [Castalia](https://github.com/boulis/Castalia) 5.x


## Contributing

Please read our [contributing instructions](CONTRIBUTING.md) and our [code of conduct](CODE_OF_CONDUCT.md),
for details on the process of submitting requests to us.

## Versioning

We use [SemVer](https://semver.org/) for versioning. For the versions available, see the tags on this repository.

## Authors

* Francesco Racciatti

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) for details.

## Acknowledgments

* Marco Tiloca
* Gianluca Dini
* Alessandro Pischedda
